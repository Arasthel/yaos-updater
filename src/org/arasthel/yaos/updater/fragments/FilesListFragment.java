package org.arasthel.yaos.updater.fragments;

import java.io.File;
import java.io.FileFilter;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;

import org.arasthel.yaos.updater.R;
import org.arasthel.yaos.updater.interfaces.FileExpandableListAdapter;
import org.arasthel.yaos.updater.interfaces.InstallListItem;
import org.arasthel.yaos.updater.interfaces.Page;
import org.arasthel.yaos.updater.utils.Constants;
import org.arasthel.yaos.updater.utils.DataStorage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Toast;

/**
 * The Class FilesListFragment.
 */
public class FilesListFragment extends Fragment{

	/** The ExpandableListView list. */
	private ExpandableListView list;
	
	private BroadcastReceiver refreshReceiver;
	
	/**
	 * Instantiates a new files list fragment.
	 */
	public FilesListFragment() {
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_install_file_list, null);
		list = (ExpandableListView) v.findViewById(R.id.install_files_list);
		list.setEmptyView(v.findViewById(R.id.empty_view));
		list.setOnChildClickListener(new OnChildClickListener() {
			
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				
				CheckedTextView textView = (CheckedTextView) v.findViewById(android.R.id.text1);
				textView.setChecked(!textView.isChecked());
				FileExpandableListAdapter adapter = ((FileExpandableListAdapter) list.getExpandableListAdapter());
				ArrayList<SimpleEntry<String, String>> checked = adapter.getChecked();
				SimpleEntry<String, String> auxEntry = new SimpleEntry<String, String>(((Page) adapter.getGroup(groupPosition)).getType(), ((File) adapter.getChild(groupPosition, childPosition)).getName());
				if(checked.contains(auxEntry)){
					checked.remove(auxEntry);
				}else{
					checked.add(auxEntry);
				}
				adapter.setChecked(checked);
				return true;
			}
		});
		Button delete = (Button) v.findViewById(R.id.install_file_list_delete);
		delete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				deleteFiles();
			}
		});
		
		Button add = (Button) v.findViewById(R.id.install_file_list_addto);
		if(DataStorage.justReboot){
			add.setVisibility(View.GONE);
		}
		add.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				addFiles();
			}
		});
		return v;
	}
	
	/**
	 * Refresh list with new files.
	 */
	private void refreshList(){
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
		String downloadDir = sp.getString("download_dir", Constants.downloadDir);
		if(!downloadDir.endsWith("/")){
			downloadDir += "/";
		}
		File dir = new File(downloadDir);
		if(!dir.exists()){
			return;
		}
		File[] categoriesDirs = dir.listFiles(new FileFilter() {
			
			@Override
			public boolean accept(File arg0) {
				return arg0.isDirectory();
			}
		});
		ArrayList<Page> pagesList = new ArrayList<Page>();
		for(File f : categoriesDirs){
			for(int i = 0; i < DataStorage.pages.size(); i++){
				if(DataStorage.pages.get(i).getType().equals(f.getName())){
					pagesList.add(DataStorage.pages.get(i));
				}
			}
		}
		File[][] items = new File[pagesList.size()][];
		for(int i = 0; i < pagesList.size(); i++){
			File f = new File(downloadDir+pagesList.get(i).getType());
			items[i] = f.listFiles(new FileFilter() {
				
				@Override
				public boolean accept(File arg0) {
					return arg0.getName().endsWith(".zip");
				}
			});
		}
		FileExpandableListAdapter adapter = new FileExpandableListAdapter(getActivity(), pagesList.toArray(new Page[pagesList.size()]), items);
		list.setAdapter(adapter);
		for(int i = 0; i < list.getExpandableListAdapter().getGroupCount(); i++){
			list.expandGroup(i);
		}
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		refreshReceiver = new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				refreshList();
			}
		};
		getActivity().registerReceiver(refreshReceiver, new IntentFilter(Constants.REFRESH_LIST));
		refreshList();
		super.onResume();
	}
	
	@Override
	public void onPause() {
		getActivity().unregisterReceiver(refreshReceiver);
		super.onPause();
	}
	
	/**
	 * Adds the files to the list.
	 */
	private void addFiles(){
		FragmentManager fm = ((FragmentActivity) getActivity()).getSupportFragmentManager();
		InstallFragment installFragment = (InstallFragment) fm.findFragmentById(R.id.install_container);
		if(installFragment == null){
			installFragment = (InstallFragment) fm.findFragmentById(R.id.right_column);
		}
		FileExpandableListAdapter adapter = (FileExpandableListAdapter) list.getExpandableListAdapter();
		ArrayList<SimpleEntry<String, String>> selected = adapter.getChecked();
		for(SimpleEntry<String, String> entry : selected){
			Page p = null;
			for(int i = 0; i < DataStorage.pages.size(); i++){
				if(DataStorage.pages.get(i).getType().equals(entry.getKey())){
					p = DataStorage.pages.get(i);
				}
			}
			InstallListItem item = new InstallListItem(InstallListItem.FILE, entry.getValue(), p);
			installFragment.addItem(item);
			
		}
	}
	
	/**
	 * Delete selected files.
	 */
	private void deleteFiles(){
		if(list != null){
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
			String path = sp.getString("download_dir", Constants.downloadDir);
			if(!path.endsWith("/")){
				path += "/";
			}
			FileExpandableListAdapter adapter = (FileExpandableListAdapter) list.getExpandableListAdapter();
			ArrayList<SimpleEntry<String, String>> selected = adapter.getChecked();
			boolean deleted = false;
			for(SimpleEntry<String, String> entry : selected){
				File f = new File(path+entry.getKey(),entry.getValue());
				if(f.exists()){
					deleted = true;
					f.delete();
				}
			}
			if(deleted){
				Toast.makeText(getActivity(), getResources().getString(R.string.files_deleted), Toast.LENGTH_SHORT).show();
				refreshList();
			}
		}
	}
	
}
