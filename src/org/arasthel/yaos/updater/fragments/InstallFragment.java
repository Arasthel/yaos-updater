package org.arasthel.yaos.updater.fragments;

import java.util.Collection;

import org.arasthel.yaos.updater.R;
import org.arasthel.yaos.updater.interfaces.DraggableListView;
import org.arasthel.yaos.updater.interfaces.DraggableListView.DropListener;
import org.arasthel.yaos.updater.interfaces.InstallListAdapter;
import org.arasthel.yaos.updater.interfaces.InstallListItem;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * The Class InstallFragment.
 */
public class InstallFragment extends Fragment{
	
	/** The list of zips to apply. */
	private DraggableListView list;
	
	/**
	 * Instantiates a new install fragment.
	 */
	public InstallFragment() {
		// TODO Auto-generated constructor stub
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_install_layout, null);
		list = (DraggableListView) v.findViewById(R.id.install_list);
		list.setOnCreateContextMenuListener(getActivity());
		list.setEmptyView(v.findViewById(R.id.empty_view));
		InstallListAdapter adapter = new InstallListAdapter(getActivity(), R.layout.list_item);
		list.setAdapter(adapter);
		list.setDropListener(mDropListener);
		
		Button addWipe = (Button) v.findViewById(R.id.wipe);
		addWipe.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((InstallListAdapter) list.getAdapter()).add(new InstallListItem(InstallListItem.WIPE, getResources().getString(R.string.wipe), null));
				((InstallListAdapter) list.getAdapter()).notifyDataSetChanged();
			}
		});
		Button addBackup = (Button) v.findViewById(R.id.backup);
		addBackup.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle(getResources().getString(R.string.backup));
				final EditText backupName = new EditText(getActivity());
				builder.setView(backupName);
				builder.setPositiveButton(getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						((InstallListAdapter) list.getAdapter()).add(new InstallListItem(InstallListItem.BACKUP, backupName.getText().toString(), null));
						((InstallListAdapter) list.getAdapter()).notifyDataSetChanged();
					}
				});
				builder.setNegativeButton(getResources().getString(R.string.cancel), null);
				builder.show();
			}
		});
		
		
		return v;
	}

    /** The drop item listener. */
	private DraggableListView.DropListener mDropListener = new DropListener() {
		
		@Override
		public void drop(int from, int to) {
			if(from == to){
				return;
			}
			((InstallListAdapter) list.getAdapter()).moveItemToPosition(from, to);
		}
	};
	
	/**
	 * Adds the item.
	 *
	 * @param o the o
	 */
	public void addItem(Object o) {
		InstallListAdapter adapter = (InstallListAdapter) list.getAdapter();
		adapter.add((InstallListItem) o);
	}
	
	/**
	 * Adds a collection of items.
	 *
	 * @param collection the collection
	 */
	public void addItems(Collection<? extends InstallListItem> collection){
		InstallListAdapter adapter = (InstallListAdapter) list.getAdapter();
		adapter.addAll(collection);
	}
	
}
