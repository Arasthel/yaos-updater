package org.arasthel.yaos.updater.fragments;

import java.util.ArrayList;

import org.arasthel.yaos.updater.Main;
import org.arasthel.yaos.updater.R;
import org.arasthel.yaos.updater.interfaces.CustomHighLightAdapter;
import org.arasthel.yaos.updater.interfaces.Update;
import org.arasthel.yaos.updater.utils.DataStorage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

// TODO: Auto-generated Javadoc
/**
 * The Class OnlineListFragment.
 */
public class OnlineListFragment extends Fragment{

	/** The adapter. */
	private CustomHighLightAdapter<Update> adapter;
	
	/** The list. */
	private ListView list;
	
	/** The filter. */
	private String filter;
	
	/** The type. */
	private int type = 0;
	
	/**
	 * New instance.
	 *
	 * @param type the type
	 * @return the online list fragment
	 */
	public static OnlineListFragment newInstance(int type){
		OnlineListFragment list = new OnlineListFragment();
		Bundle args = new Bundle();
		args.putInt("type", type);
		list.setArguments(args);
		return list;
	}
	
	
	/**
	 * Instantiates a new online list fragment.
	 */
	public OnlineListFragment() {
		
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_online_list, null);
		if(getArguments() != null && getArguments().containsKey("type")){
			type = getArguments().getInt("type");
		}
		return v;
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onStart()
	 */
	@Override
	public void onStart() {
		list  = (ListView) getView().findViewById(R.id.online_files_list);
		list.setEmptyView(getView().findViewById(R.id.empty_view));
		adapter = new CustomHighLightAdapter<Update>(getActivity(), R.layout.list_item, DataStorage.pages.get(type).getType());
		updateContent();
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				addSelected(list, arg2);
			}
		});
		View title = getView().findViewById(R.id.category_title);
		title.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				addSelected(list, -1);
			}
		});
		if(DataStorage.selected.containsKey(DataStorage.pages.get(type))){
			int selected = DataStorage.selected.get(type);
			addSelected(list, selected);
		}
		
		Spinner filterSpinner = (Spinner) getView().findViewById(R.id.filters);
		filterSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				String[] filterValues = getResources().getStringArray(R.array.filters_values);
				filter = filterValues[arg2];
				updateContent();
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		super.onStart();
	}

	/**
	 * Adds the selected.
	 *
	 * @param list the list
	 * @param selected the selected
	 */
	private void addSelected(ListView list, int selected){
		if(DataStorage.selected.containsKey(type)){
			DataStorage.selected.remove(type);
		}
		if(selected > -1){
			DataStorage.selected.put(DataStorage.pages.get(type).getType(), selected);
		}
		Main activity = (Main) getActivity();
		activity.updateRightColumnContent(DataStorage.pages.get(type));
		((ArrayAdapter) list.getAdapter()).notifyDataSetInvalidated();
	}
	
	/**
	 * Update content.
	 */
	public void updateContent(){
		if(getView() != null){
			TextView category = (TextView) getView().findViewById(R.id.category_title);
	    	if(category != null){
	    		category.setText(DataStorage.pages.get(type).getName());
	    	}
	    	Spinner filterSpinner = (Spinner) getView().findViewById(R.id.filters);
	    	String[] filterValues = getResources().getStringArray(R.array.filters_values);
	    	filter = filterValues[filterSpinner.getSelectedItemPosition()];
		}
		if(adapter == null){
			if(getActivity() == null){
				return;
			}else{
				adapter = new CustomHighLightAdapter<Update>(getActivity(), R.layout.list_item, DataStorage.pages.get(type).getType());
			}
		}else{
			adapter.setType(DataStorage.pages.get(type).getType());
		}
		adapter.clear();
		if(DataStorage.updates.containsKey(DataStorage.pages.get(type).getType())){
			ArrayList<Update> updates = DataStorage.updates.get(DataStorage.pages.get(type).getType());
			ArrayList<Update> selectedUpdates = new ArrayList<Update>();
			String[] filterValues = getResources().getStringArray(R.array.filters_values);
			if(filter.equals(filterValues[2])){
				// ALL
				selectedUpdates = updates;
			}else if(filter.equals(filterValues[0])){
				// NEW
				for(Update update : updates){
					if(update.canApplyTo(DataStorage.myVersion)){
						selectedUpdates.add(update);
					}
				}
			}else if(filter.equals(filterValues[1])){
				// STABLE
				for(Update update : updates){
					if(update.canApplyTo(DataStorage.myVersion) && update.isStable()){
						selectedUpdates.add(update);
					}
				}
			}
			adapter.addAll(selectedUpdates);
		}
		if(!adapter.isEmpty()){
			list.setAdapter(adapter);
		}
	}
	
	/**
	 * Sets the type of the updates.
	 *
	 * @param i the new type of the updates
	 */
	public void setType(int i){
		this.type = i;
	}
	
	/**
	 * Gets the type of the updates.
	 *
	 * @return the type of the updates
	 */
	public int getType(){
		return type;
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		updateContent();
		super.onResume();
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#toString()
	 */
	@Override
	public String toString() {
		return DataStorage.pages.get(type).toString();
	}
}
