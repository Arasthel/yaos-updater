package org.arasthel.yaos.updater.fragments;

import java.io.File;
import java.util.ArrayList;

import org.arasthel.yaos.updater.R;
import org.arasthel.yaos.updater.interfaces.Update;
import org.arasthel.yaos.updater.utils.Constants;
import org.arasthel.yaos.updater.utils.DataStorage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;

// TODO: Auto-generated Javadoc
/**
 * The Class DescriptionFragment.
 */
public class DescriptionFragment extends android.support.v4.app.Fragment{
	
	/** The refresh description receiver. */
	private BroadcastReceiver refreshReceiver;
	
	/**
	 * Instantiates a new description fragment.
	 */
	public DescriptionFragment() {
		// TODO Auto-generated constructor stub
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_description, null);
		return v;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onStart()
	 */
	@Override
	public void onStart() {
		String type = DataStorage.pages.get(DataStorage.currentPage).getType();
		if(DataStorage.selected.containsKey(type)){
			int selected = DataStorage.selected.get(type);
			ArrayList<Update> updates = DataStorage.updates.get(type);
			if(selected >= 0){
				updateContent(updates.get(selected));
			}
		}
		super.onStart();
	}
	
	/**
	 * Update content of the description fragment with the one from the update.
	 *
	 * @param update the update that contains the info
	 */
	public void updateContent(Update update){
		WebView htmlView = (WebView) getView().findViewById(R.id.description_webview);
		htmlView.loadData(update.getDescription(), "text/html", "UTF-8");
		File file = new File(DataStorage.downloadPath, update.getFilename());
		Button addTo = (Button) getView().findViewById(R.id.add_to_install_list);
		addTo.setEnabled(file.exists());
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		refreshReceiver = new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
				String type = DataStorage.pages.get(DataStorage.currentPage).getType();
				if(DataStorage.selected.containsKey(type)){
					int position = DataStorage.selected.get(type);
					updateContent(DataStorage.updates.get(type).get(position));
				}
			}
		};
		if(getActivity() != null){
			getActivity().registerReceiver(refreshReceiver, new IntentFilter(Constants.REFRESH_LIST));
		}
		super.onResume();
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onPause()
	 */
	@Override
	public void onPause() {
		if(getActivity() != null){
			getActivity().unregisterReceiver(refreshReceiver);
		}	
		super.onPause();
	}
}
