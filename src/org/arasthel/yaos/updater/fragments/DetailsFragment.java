package org.arasthel.yaos.updater.fragments;

import java.io.File;
import java.util.ArrayList;

import org.arasthel.yaos.updater.R;
import org.arasthel.yaos.updater.interfaces.Update;
import org.arasthel.yaos.updater.tasks.CheckMD5Task;
import org.arasthel.yaos.updater.tasks.DownloadFileTask;
import org.arasthel.yaos.updater.tasks.GetMD5Task;
import org.arasthel.yaos.updater.utils.Constants;
import org.arasthel.yaos.updater.utils.DataStorage;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * The Class DetailsFragment.
 */
public class DetailsFragment extends android.support.v4.app.Fragment{
	
	/**
	 * Instantiates a new details fragment.
	 */
	public DetailsFragment() {
	}
	
	/** The update. */
	private Update update;
	
	/** The refresh receiver. */
	private BroadcastReceiver refreshReceiver;
	
	/** The disabled actions receiver. */
	private BroadcastReceiver disabledActionsReceiver;
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_details, null);
		return v;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onStart()
	 */
	@Override
	public void onStart() {
		String type = DataStorage.pages.get(DataStorage.currentPage).getType();
		if(DataStorage.selected.containsKey(type)){
			int selected = DataStorage.selected.get(type);
			ArrayList<Update> updates = DataStorage.updates.get(type);
			if(selected >= 0){
				updateContent(updates.get(selected));
			}
		}
		super.onStart();
	}
	
	/**
	 * Update content with the data of the update.
	 *
	 * @param update the update
	 */
	public void updateContent(Update update){
		this.update = update;
		String path = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("download_dir", Constants.downloadDir);
		if(!path.endsWith("/")){
			path += "/";
		}
		path += update.getCategory().getType() + "/";
		TextView nameView = (TextView) getView().findViewById(R.id.name);
		nameView.setText(update.getName());
		TextView type = (TextView) getView().findViewById(R.id.type);
		type.setText(update.getCategory().getName());
		TextView filename = (TextView) getView().findViewById(R.id.filename);
		filename.setText(update.getFilename());
		TextView version = (TextView) getView().findViewById(R.id.version);
		version.setText(update.getVersionString(update.getVersion()));
		TextView stable = (TextView) getView().findViewById(R.id.is_stable);
		if(update.isStable()){
			stable.setText(getString(R.string.yes));
		}else{
			stable.setText(getString(R.string.no));
		}
		TextView uploader = (TextView) getView().findViewById(R.id.uploader_name);
		uploader.setText(update.getUploader());
		
		// Actions
		
		ImageView checkMD5 = (ImageView) getView().findViewById(R.id.check_md5);
		ImageView downloadMD5 = (ImageView) getView().findViewById(R.id.download_md5);
		ImageView deleteFile = (ImageView) getView().findViewById(R.id.delete_file);
		File md5 = new File(path+update.getFilename()+".md5");
		File file = new File(path+update.getFilename());
		downloadMD5.setImageResource(R.drawable.download_md5);
		downloadMD5.setOnClickListener(this.downloadMD5);
		if(md5.exists()){
			if(file.exists()){
				checkMD5.setImageResource(R.drawable.check_md5);
				checkMD5.setOnClickListener(this.checkMD5);
			}else{
				checkMD5.setImageResource(R.drawable.check_md5_disabled);
				checkMD5.setOnClickListener(null);
			}
		}else{
			checkMD5.setImageResource(R.drawable.check_md5_disabled);
			checkMD5.setOnClickListener(null);
		}
		if(file.exists()){
			deleteFile.setImageResource(R.drawable.delete_zip);
			deleteFile.setOnClickListener(deleteZip);
		}else{
			deleteFile.setImageResource(R.drawable.delete_zip_disabled);
			deleteFile.setOnClickListener(null);
		}
		if(DownloadFileTask.tasks.containsKey(update.getFilename())){
			toggleActionsEnabled(false);
		}
	}
	
	/** Verify the MD5 of the file. */
	private OnClickListener checkMD5 = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			String path = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("download_dir", Constants.downloadDir);
			if(!path.endsWith("/")){
				path += "/";
			}
			new CheckMD5Task(getActivity(), path+update.getCategory().getType()+"/"+update.getFilename()).execute();
		}
	};
	
	/** Download the MD5 file for this update. */
	private OnClickListener downloadMD5 = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			String path = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("download_dir", Constants.downloadDir);
			Toast.makeText(getActivity(), getString(R.string.md5_downloading), Toast.LENGTH_SHORT).show();
			new GetMD5Task(getActivity(), path, update, DataStorage.pages.get(DataStorage.currentPage).getType()).execute();
		}
	};
	
	/** Delete the zip file. */
	private OnClickListener deleteZip = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(getResources().getString(R.string.sure_delete));
			builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					String path = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("download_dir", Constants.downloadDir)+"/"+update.getCategory().getType()+"/";
					File f = new File(path+update.getFilename());
					if(f.exists()){
						f.delete();
					}
					f = new File(path+update.getFilename()+".md5");
					if(f.exists()){
						f.delete();
						Toast.makeText(getActivity(), getResources().getString(R.string.file_deleted), Toast.LENGTH_SHORT).show();
					}else{
						
					}
					updateContent(update);
				}
			});
			builder.setNegativeButton(getResources().getString(R.string.no), null);
			builder.show();
		}
	};

	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		refreshReceiver = new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
				String type = DataStorage.pages.get(DataStorage.currentPage).getType();
				if(DataStorage.selected.containsKey(type)){
					int position = DataStorage.selected.get(type);
					updateContent(DataStorage.updates.get(type).get(position));
				}
			}
		};
		disabledActionsReceiver = new BroadcastReceiver(){

			@Override
			public void onReceive(Context context, Intent intent) {
				if(DownloadFileTask.tasks.containsKey(update.getFilename())){
					toggleActionsEnabled(false);
				}
			}
			
		};
		if(getActivity() != null){
			getActivity().registerReceiver(refreshReceiver, new IntentFilter(Constants.REFRESH_LIST));
			getActivity().registerReceiver(disabledActionsReceiver, new IntentFilter(Constants.DOWNLOAD_START));
		}
		super.onResume();
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onPause()
	 */
	@Override
	public void onPause() {
		if(getActivity() != null){
			getActivity().unregisterReceiver(refreshReceiver);
			getActivity().unregisterReceiver(disabledActionsReceiver);
		}	
		super.onPause();
	}
	
	/**
	 * Toggle actions between enabled and disabled state, according to "options" value
	 *
	 * @param option whether the options should be enabled or not
	 */
	private void toggleActionsEnabled(boolean option){
		ImageView checkMD5 = (ImageView) getView().findViewById(R.id.check_md5);
		ImageView downloadMD5 = (ImageView) getView().findViewById(R.id.download_md5);
		ImageView deleteFile = (ImageView) getView().findViewById(R.id.delete_file);
		if(option){
			updateContent(update);
		}else{
			checkMD5.setImageResource(R.drawable.check_md5_disabled);
			checkMD5.setOnClickListener(null);
			downloadMD5.setImageResource(R.drawable.download_md5_disabled);
			downloadMD5.setOnClickListener(null);
			deleteFile.setImageResource(R.drawable.delete_zip_disabled);
			deleteFile.setOnClickListener(null);
		}
	}
	
}
