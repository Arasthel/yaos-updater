package org.arasthel.yaos.updater;

import java.util.ArrayList;

import org.arasthel.yaos.updater.fragments.DescriptionFragment;
import org.arasthel.yaos.updater.fragments.DetailsFragment;
import org.arasthel.yaos.updater.fragments.FilesListFragment;
import org.arasthel.yaos.updater.fragments.OnlineListFragment;
import org.arasthel.yaos.updater.interfaces.DraggableListView;
import org.arasthel.yaos.updater.interfaces.InstallListAdapter;
import org.arasthel.yaos.updater.interfaces.InstallListItem;
import org.arasthel.yaos.updater.interfaces.Page;
import org.arasthel.yaos.updater.interfaces.Update;
import org.arasthel.yaos.updater.interfaces.ViewPagerAdapter;
import org.arasthel.yaos.updater.tasks.ApplyUpdateTask;
import org.arasthel.yaos.updater.tasks.DownloadFileTask;
import org.arasthel.yaos.updater.tasks.GetJSONTask;
import org.arasthel.yaos.updater.utils.Constants;
import org.arasthel.yaos.updater.utils.DataStorage;
import org.arasthel.yaos.updater.utils.Utils;
import org.arasthel.yaos.updater.utils.XMLPreferencesParser;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract.Contacts.Data;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SlidingDrawer;
import android.widget.Toast;
import android.widget.ViewFlipper;

// TODO: Auto-generated Javadoc
/**
 * The Class Main.
 */
public class Main extends FragmentActivity {
	
	/** The types (categories). */
	public static String[] types;
	
	/** The types values (names, to show in the app). */
	public static String[] typesValues;
	
	/** The View Pager used in phone mode. */
	private ViewPager pager;
	
	/** A boolean to check if the SlidingDrawer of phone mode is opened. */
	private boolean slidingOpened = false;
	
	/** The json loaded receiver. */
	private BroadcastReceiver jsonLoadedReceiver;
	
    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	Utils.loadPages(savedInstanceState);
		DataStorage.jsonURL = Constants.jsonURL;
        types = getResources().getStringArray(R.array.types);
		typesValues = getResources().getStringArray(R.array.types_values);
        for(int i = 0; i < types.length; i++){
        	Page page = new Page(typesValues[i], i, types[i]);
        	DataStorage.pages.add(page);
        }
        XMLPreferencesParser parser = new XMLPreferencesParser("/system/updater.xml");
        parser.parse();
        if(DataStorage.myVersion == null){
        	DataStorage.myVersion = new Update(null, "0", null, null, true, null, null, null);
        }
        Page installPage = new Page("INSTALL", DataStorage.pages.size(), DataStorage.pages.get(DataStorage.pages.size()-1).getName());
        DataStorage.pages.add(installPage);
        setContentView(R.layout.main_layout);
        pager = (ViewPager) findViewById(R.id.pager);
        if(pager == null){
        	DataStorage.isTabletScreen = true;
        }
        
        if(DataStorage.isTabletScreen){
        	OnlineListFragment onlineList = new OnlineListFragment();
        	onlineList.setType(0);
        	getSupportFragmentManager().beginTransaction().add(R.id.list_container, onlineList).commit();
        	//	If device is a tablet, use actionbar
        	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        	ActionBar actionBar = getActionBar();
    		for(int i = 0; i < DataStorage.pages.size(); i++){
    			final Page page = DataStorage.pages.get(i);
    			Tab tab = getActionBar().newTab();
    			tab.setText(page.getName());
    			tab.setTabListener(new TabListener() {
					
					@Override
					public void onTabUnselected(Tab tab, FragmentTransaction ft) {
					}
					
					@Override
					public void onTabSelected(Tab tab, FragmentTransaction ft) {
						changePage(page);
						
					}
					
					@Override
					public void onTabReselected(Tab tab, FragmentTransaction ft) {
						changePage(page);
					}
				});
    			actionBar.addTab(tab);
    		}
    		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
    	}else{
    		ArrayList<Fragment> fragments = new ArrayList<Fragment>();
    		for(int i = 0; i < DataStorage.pages.size()-1; i++){
    			OnlineListFragment list = OnlineListFragment.newInstance(i);
    			fragments.add(list);
    		}
    		FilesListFragment list = new FilesListFragment();
    		fragments.add(list);
    		
    		ViewPagerAdapter adapter = new ViewPagerAdapter(this);
    		adapter.setFragments(fragments);
    		pager.setAdapter(adapter);
    		pager.setOnPageChangeListener(new OnPageChangeListener() {
				
				@Override
				public void onPageSelected(int arg0) {
					DataStorage.currentPage = arg0;
					changePage(DataStorage.pages.get(arg0));
				}
				
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onPageScrollStateChanged(int arg0) {
				}
			});
    	}
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        boolean refreshOnInit = sp.getBoolean("refresh_on_init", false);
        if(refreshOnInit){
        	refresh();
        }
        
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_principal, menu);
        return true;
    }
    
    
    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onResume()
     */
    @Override
    protected void onResume() {
    	jsonLoadedReceiver = new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
				android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
				OnlineListFragment onlineList;
				if(DataStorage.isTabletScreen){
					onlineList = (OnlineListFragment) fm.findFragmentById(R.id.list_container);
					onlineList.updateContent();
				}else{
					if(DataStorage.currentPage != DataStorage.pages.size()-1){
						onlineList = (OnlineListFragment) ((ViewPagerAdapter) pager.getAdapter()).getItem(DataStorage.currentPage);
						onlineList.updateContent();
					}
				}
				if(DataStorage.selected.containsKey(DataStorage.currentPage)){
					DetailsFragment details = (DetailsFragment) fm.findFragmentById(R.id.details);
					int position = DataStorage.selected.get(DataStorage.currentPage);
					details.updateContent(DataStorage.updates.get(DataStorage.currentPage).get(position));
				}
			}
		};
		registerReceiver(jsonLoadedReceiver, new IntentFilter(Constants.REFRESH_LIST));
    	super.onResume();
    }
    
    
    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onStart()
     */
    @Override
    protected void onStart() {
    	if(pager != null){
	    	pager.setCurrentItem(DataStorage.currentPage);
			updateDots();
    	}
    	super.onStart();
    }
    
    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onPause()
     */
    @Override
    protected void onPause() {
    	unregisterReceiver(jsonLoadedReceiver);
    	super.onPause();
    }
    

    /**
     * Change page.
     *
     * @param page the page to change to
     */
    private void changePage(Page page){
    	int lastPage = Integer.valueOf(DataStorage.currentPage);
    	DataStorage.currentPage = page.getIndex();
    	android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
		if(DataStorage.isTabletScreen){
			if(lastPage != DataStorage.pages.size()-1 && page.getIndex() == DataStorage.pages.size()-1){
				//We're getting into Install Layout
				ViewFlipper flipper = (ViewFlipper) findViewById(R.id.flipper);
				flipper.showNext();
			}else if(lastPage == DataStorage.pages.size()-1 && page.getIndex() != DataStorage.pages.size()-1){
				//We're going back from Install to any other page
				ViewFlipper flipper = (ViewFlipper) findViewById(R.id.flipper);
				flipper.showPrevious();
				OnlineListFragment onlineList = (OnlineListFragment) fm.findFragmentById(R.id.list_container);
				if(onlineList != null){
					onlineList.setType(DataStorage.currentPage);
				}
				updateRightColumnContent(page);
			}else{
				OnlineListFragment onlineList = (OnlineListFragment) fm.findFragmentById(R.id.list_container);
				if(onlineList != null){
					onlineList.setType(DataStorage.currentPage);
				}
				updateRightColumnContent(page);
			}
		}else{
			if(DataStorage.currentPage != DataStorage.pages.size()-1){
				OnlineListFragment onlineList = (OnlineListFragment) ((ViewPagerAdapter) pager.getAdapter()).getItem(DataStorage.currentPage);
				if(onlineList != null){
					onlineList.updateContent();
				}
			}
			updateDots();
		}
		invalidateOptionsMenu();
    }
    
    /**
     * Update right column content.
     *
     * @param page the page to get the info from
     */
    public void updateRightColumnContent(Page page){
    	String type = page.getType();
    	View welcomeText = findViewById(R.id.yaos_info);
    	View info = findViewById(R.id.update_info);
    	if(welcomeText != null && info != null){
    		if(DataStorage.selected.containsKey(type)){
        		int selected = DataStorage.selected.get(type);
        		Update update = DataStorage.updates.get(type).get(selected);
        		welcomeText.setVisibility(View.GONE);
            	info.setVisibility(View.VISIBLE);
            	DetailsFragment details = (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.details);
            	details.updateContent(update);
            	DescriptionFragment description = (DescriptionFragment) getSupportFragmentManager().findFragmentById(R.id.description);
            	description.updateContent(update);
        	}else{
        		welcomeText.setVisibility(View.VISIBLE);
            	info.setVisibility(View.GONE);
        	}
    	}
    	
    }
    
    /**
     * Refresh action.
     *
     * @return true, if successful
     */
    public boolean refresh(){
    	ProgressDialog progressDialog = new ProgressDialog(this);
    	progressDialog.setIndeterminate(true);
    	progressDialog.setMessage(getResources().getString(R.string.loading_json));
    	progressDialog.show();
		new GetJSONTask(progressDialog).execute(this);
    	return true;
    }
    
    /**
     * Download file action.
     *
     * @return true, if successful
     */
    public boolean download(){
    	if(DataStorage.selected.containsKey(DataStorage.pages.get(DataStorage.currentPage).getType())){
    		int selected = DataStorage.selected.get(DataStorage.pages.get(DataStorage.currentPage).getType());
    		Update update = DataStorage.updates.get(DataStorage.pages.get(DataStorage.currentPage).getType()).get(selected);
    		if(!DownloadFileTask.tasks.containsKey(update.getFilename())){
    			new DownloadFileTask(this, update.getFilename(), DataStorage.pages.get(DataStorage.currentPage).getType()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    		}else{
    			Toast.makeText(this, getResources().getString(R.string.already_downloaded), Toast.LENGTH_SHORT).show();
    		}
    	}
    	return true;
    }
    
    /**
     * Open settings action.
     *
     * @return true, if successful
     */
    public boolean openSettings(){
    	Intent i = new Intent(this, Preferences.class);
    	startActivity(i);
    	return true;
    }
    
    /**
     * Update dots in phone mode, updates the dots layout.
     */
    private void updateDots(){
    	LinearLayout dotContainer = (LinearLayout) findViewById(R.id.dot_container);
		if(dotContainer != null){
			dotContainer.removeAllViews();
			for(Page p : DataStorage.pages){
				ImageView dot = new ImageView(this);
				if(p.getIndex() == DataStorage.currentPage){
					dot.setImageResource(R.drawable.dot_highlighted);
				}else{
					dot.setImageResource(R.drawable.dot_disabled);
				}
				float scale = getResources().getDisplayMetrics().density;
				int padding = (int) ((int) 4*scale);
				dot.setPadding(padding, 0, padding, 0);
				dotContainer.addView(dot);
			}
		}
    }
    
    /* (non-Javadoc)
     * @see android.app.Activity#onPrepareOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	SlidingDrawer drawer = (SlidingDrawer) findViewById(R.id.install_drawer);
    	boolean option = DataStorage.currentPage == DataStorage.pages.size()-1;
    	MenuItem download = menu.findItem(R.id.download);
		download.setVisible(!option);
		MenuItem info = menu.findItem(R.id.info);
		if(info != null){
			info.setVisible(!option);
		}
		MenuItem refresh = menu.findItem(R.id.refresh);
		refresh.setVisible(!option);
		MenuItem install = menu.findItem(R.id.install);
		if(install != null){
			install.setVisible(option);
		}
		MenuItem toggleDrawer = menu.findItem(R.id.toggle_sliding_drawer);
		if(toggleDrawer != null && !DataStorage.justReboot){
			toggleDrawer.setVisible(option);
		}
		if(drawer != null){
			if(slidingOpened){
				toggleDrawer.setTitle(getString(R.string.close_sliding));
				toggleDrawer.setIcon(android.R.drawable.arrow_down_float);
			}else{
				toggleDrawer.setTitle(getString(R.string.open_sliding));
				toggleDrawer.setIcon(android.R.drawable.arrow_up_float);
			}
		}
    	return super.onPrepareOptionsMenu(menu);
    }
    
    /* (non-Javadoc)
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case(R.id.download):
			download();
			break;
		case(R.id.info):
			Intent i = new Intent(Main.this, UpdateInfo.class);
			startActivity(i);
			break;
		case(R.id.refresh):
			refresh();
			break;
		case(R.id.menu_settings):
			openSettings();
			break;
		case(R.id.toggle_sliding_drawer):
			SlidingDrawer drawer = (SlidingDrawer) findViewById(R.id.install_drawer);
			slidingOpened = !drawer.isOpened();
			invalidateOptionsMenu();
			drawer.animateToggle();
			break;
		case(R.id.install):
			ArrayList<InstallListItem> installItems = null;
			DraggableListView list = (DraggableListView) findViewById(R.id.install_list);
			if(list != null){
				installItems = ((InstallListAdapter) list.getAdapter()).getList();
			}
			new ApplyUpdateTask(this, installItems).execute(this);
		}
		return super.onOptionsItemSelected(item);
	}
 
    @Override
	protected void onSaveInstanceState(Bundle outState) {
		Utils.savePages(outState);
		super.onSaveInstanceState(outState);
	}
    
    @Override
    public void onBackPressed() {
    	SlidingDrawer drawer = (SlidingDrawer) findViewById(R.id.install_drawer);
    	if(drawer != null && slidingOpened){
    		drawer.animateClose();
    		slidingOpened = false;
    		invalidateOptionsMenu();
    	}else{
    		super.onBackPressed();
    	}
    }
}

