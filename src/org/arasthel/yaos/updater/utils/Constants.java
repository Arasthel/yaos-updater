package org.arasthel.yaos.updater.utils;

import android.os.Environment;

/**
 * Application Constants. Most of them can be override with values in /system/updater.xml file
 */
public class Constants {

	/** Version separator character. <p>In example: "."	","	"-"	"_" */
	public static String versionSeparator = ".";
	
	/** Default JSON URL */
	public static String jsonURL = "http://content.wuala.com/contents/Arasthel/Android/YAOS/JSON/prueba.json";
	
	/** Default download folder (usually "/sdcard/") */
	public static String downloadDir = Environment.getExternalStorageDirectory().getPath();
	
	/** The version property line to look for in /system/build.prop to get the current ROM version. */
	public static String VERSION_PROPERTY = "yaos.update.version";
	
	// Default Preferences XML path
	/** The preferences path. */
	public static String preferencesPath = "/system/updater.xml";
	
	/** The refresh list intent value - DO NOT TOUCH IF YOU DON'T KNOW WHAT YOU ARE DOING */
	public static String REFRESH_LIST = "org.arasthel.yaos.updater.REFRESH_LIST";
	
	/** The download start intent value - DO NOT TOUCH IF YOU DON'T KNOW WHAT YOU ARE DOING */
	public static String DOWNLOAD_START = "org.arasthel.yaos.updater.DOWNLOAD_START";
	
	/** The cancel download intent value - DO NOT TOUCH IF YOU DON'T KNOW WHAT YOU ARE DOING */
	public static String CANCEL_DOWNLOAD = "org.arasthel.yaos.updater.CANCEL_DOWNLOAD";
	
}
