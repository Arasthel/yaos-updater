package org.arasthel.yaos.updater.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

import org.arasthel.yaos.updater.interfaces.Page;
import org.arasthel.yaos.updater.interfaces.Update;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import android.sax.Element;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.sax.TextElementListener;
import android.util.Xml;

/**
 * The Class XMLPreferencesParser.
 */
public class XMLPreferencesParser {
	
	/** The path of the preferences xml. */
	private String path;
	
	/** The auxiliar page variable. */
	private Page p;
	
	/** The page index. */
	private int index = 0;

	/**
	 * Instantiates a new XML preferences parser using the Constants value for the XML.
	 */
	public XMLPreferencesParser(){
		this.path = Constants.preferencesPath;
	}
	
	/**
	 * Instantiates a new XML preferences parser.
	 *
	 * @param path the path
	 */
	public XMLPreferencesParser(String path){
		this.path = path;
	}
	
	/**
	 * Parses the XML file.
	 */
	public void parse(){
		RootElement root = new RootElement("preferences");
		Element categories = root.getChild("categories");
		Element category = categories.getChild("category");
		String name = null;
		final String locale = Locale.getDefault().getLanguage();
		final String localeCountry = locale+"_"+Locale.getDefault().getCountry();
		// Override categories with custom ones
		categories.setStartElementListener(new StartElementListener() {
			
			@Override
			public void start(Attributes attributes) {
				DataStorage.pages.clear();
			}
		});
		
		// Loading categories
		
		// Creating new category with its index
		category.setStartElementListener(new StartElementListener() {
			
			@Override
			public void start(Attributes attributes) {
				p = new Page(null, index, null);
				index++;
			}
		});
		
		//Adding the category to the list
		category.setEndElementListener(new EndElementListener() {
			
			@Override
			public void end() {
				DataStorage.pages.add(p);
			}
		});
		
		// Configuring category name according to Locale
		category.getChild("name").setTextElementListener(new TextElementListener() {
			
			boolean isMyLocale = false;
			
			@Override
			public void end(String arg0) {
				if(isMyLocale){
					p.setName(arg0);
					isMyLocale = false;
				}
				if(p.getName() == null){
					p.setName(arg0);
				}
			}
			
			@Override
			public void start(Attributes arg0) {
				String readLocale = arg0.getValue("locale");
				if(readLocale != null && (readLocale.equals(locale) || readLocale.equals(localeCountry))){
					isMyLocale = true;
				}
				
			}
		});
		
		// Adding category Type
		category.getChild("type").setEndTextElementListener(new EndTextElementListener() {
			
			@Override
			public void end(String body) {
				p.setType(body);
			}
		});
		
		// The internal SD mountpoint in recovery, for example "emmc" or "sdcard"
		
		root.getChild("internal_mountpoint").setEndTextElementListener(new EndTextElementListener() {
			
			@Override
			public void end(String arg0) {
				DataStorage.internalRecoveryMountpoint = arg0;
			}
		});
		
		// The internal SD mountpoint in recovery, for example "/emmc/", "/sdcard/" or "/storage/sdcard/"
		
				root.getChild("internal_path").setEndTextElementListener(new EndTextElementListener() {
					
					@Override
					public void end(String arg0) {
						DataStorage.internalRecoveryPath= arg0;
					}
				});
		
		// The external SD mountpoint in recovery, for example "sdcard"
		
		root.getChild("external_mountpoint").setEndTextElementListener(new EndTextElementListener() {
			
			@Override
			public void end(String arg0) {
				DataStorage.externalRecoveryMountpoint = arg0;
			}
		});
		
		// The external SD mountpoint in recovery, for example "/emmc/", "/sdcard/" or "/storage/sdcard/"
		
		root.getChild("external_path").setEndTextElementListener(new EndTextElementListener() {
			
			@Override
			public void end(String arg0) {
				DataStorage.externalRecoveryPath = arg0;
			}
		});

		// The JSON URL
		
		root.getChild("url_json").setEndTextElementListener(new EndTextElementListener() {
			
			@Override
			public void end(String body) {
				DataStorage.jsonURL = body;
			}
		});
		
		// ROM Version String
		
		root.getChild("version").setEndTextElementListener(new EndTextElementListener() {
			
			@Override
			public void end(String body) {
				DataStorage.myVersion = new Update(null, body, null, null, true, null, null, null);
			}
		});
		
		// "Just Reboot" option for devices that cannot use extendedcommand to install files automatically on recovery mode
		
		root.getChild("just_reboot").setEndTextElementListener(new EndTextElementListener() {
			
			@Override
			public void end(String body) {
				DataStorage.justReboot = Boolean.parseBoolean(body);
			}
		});
		
		try {
			Xml.parse(new FileInputStream(new File(path)), 
					Xml.Encoding.UTF_8,
			        root.getContentHandler());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
