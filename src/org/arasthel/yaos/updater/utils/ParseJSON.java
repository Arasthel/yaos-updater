package org.arasthel.yaos.updater.utils;

import java.util.ArrayList;

import org.arasthel.yaos.updater.interfaces.Page;
import org.arasthel.yaos.updater.interfaces.Update;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ParseJSON {

	public static void parseJSON(String jsonStr) throws JSONException{
		if(!(jsonStr != null && !jsonStr.isEmpty())){
			return;
		}
		JSONObject json = new JSONObject(jsonStr);
		
		// Parsing mirrors
		JSONArray mirrorlists = json.getJSONArray("mirrorlists");
		DataStorage.mirrors.clear();
		for(int i = 0; i < mirrorlists.length(); i++){
			ArrayList<String> mirrorsArray = new ArrayList<String>();
			//First, we navigate through categories
			JSONObject mirrors = mirrorlists.getJSONObject(i);
			String name = mirrors.getString("name");
			JSONArray list = mirrors.getJSONArray("mirrors");
			for(int j = 0; j < list.length(); j++){
				//Then, we assign mirrors to each category
				mirrorsArray.add(list.getString(j));
			}
			DataStorage.mirrors.put(name, mirrorsArray);
		}
		
		
		//Parsing updates
		DataStorage.updates.clear();
		for(int i = 0; i < DataStorage.pages.size(); i++){
			ArrayList<Update> updates = new ArrayList<Update>();
			String categoryName = DataStorage.pages.get(i).getType();
			JSONArray category = json.optJSONArray(categoryName);
			if(category == null){
				continue;
			}
			for(int j = 0; j < category.length(); j++){
				JSONObject jsonUpdate = category.getJSONObject(j);
				String name = jsonUpdate.optString("name");
				String version = jsonUpdate.optString("version");
				String versionMin = jsonUpdate.optString("version_min");
				boolean stable = jsonUpdate.optBoolean("stable");
				String filename = jsonUpdate.optString("filename");
				String description = jsonUpdate.optString("description");
				String uploader = jsonUpdate.optString("uploader");
				Page p = null;
				for(int k = 0; k < DataStorage.pages.size(); k++){
					if(DataStorage.pages.get(i).getType().equals(categoryName)){
						p = DataStorage.pages.get(i);
					}
				}
				Update update = new Update(name, version, versionMin, filename, stable, description, uploader,p);
				updates.add(update);
			}
			DataStorage.updates.put(categoryName, updates);
		}
		
	}
	
}
