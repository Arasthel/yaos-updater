package org.arasthel.yaos.updater.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import org.arasthel.yaos.updater.interfaces.Page;
import org.arasthel.yaos.updater.interfaces.ViewPagerAdapter;
import org.json.JSONException;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;

public class Utils {

	public static boolean isConnectionEnabled(Context context){
		ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] infos = conManager.getAllNetworkInfo();
		boolean connected = false;
		for(NetworkInfo netInfo : infos){
			if(netInfo.isConnectedOrConnecting()){
				connected = true;
			}
		}
		return connected;
	}
	
	public static void deleteMD5(String path){
		path += ".md5";
		File f = new File(path);
		if(f.exists()){
			f.delete();
		}
	}
	
	public static boolean checkMD5(String path) throws NoSuchAlgorithmException, IOException{
		File f = new File(path);
		path += ".md5";
		File md5 = new File(path);
		if(f.exists() && md5.exists()){
			Scanner scan = new Scanner(md5);
			String md5Str = null;
			if((md5Str = scan.findInLine("[a-zA-Z0-9]{32}")) != null){
				FileInputStream fis = new FileInputStream(f);
				MessageDigest digest = MessageDigest.getInstance("MD5");
				byte[] buffer = new byte[8192];
				int read = -1;
				while((read = fis.read(buffer)) > 0){
					digest.update(buffer, 0, read);
				}
				fis.close();
				byte[] sum = digest.digest();
				BigInteger bigInt = new BigInteger(1, sum);
				String result = bigInt.toString(16);
				result = String.format("%32s", result).replace(' ', '0');
				if(result.equals(md5Str)){
					return true;
				}else{
					return false;
				}
			}
		}else{
			return true;
		}	
		return false;
	}
	
	public static String getProp(String prop){
		String line;
        BufferedReader input = null;
        try {
            Process p = Runtime.getRuntime().exec("getprop "+prop);
            input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            line = input.readLine();
        }
        catch (IOException ex) {
            Log.e("YAOSUPDATER", "Unable to read sysprop "+prop, ex);
            return null;
        }
        finally {
            if (input != null) {
                try {
                    input.close();
                }
                catch (IOException e) {
                    Log.e("YAOSUPDATER", "Exception while closing InputStream", e);
                }
            }
        }
        if(line.isEmpty()){
        	line = "0";
        }
        return line;
	}
	
	public static Bundle savePages(Bundle savedInstance){
		savedInstance.putSerializable("pages", DataStorage.pages);
		savedInstance.putSerializable("selected", DataStorage.selected);
		savedInstance.putString("json", DataStorage.json);
		for(int i = 0; i < DataStorage.pages.size(); i++){
		}
		return savedInstance;
	}
	
	public static void loadPages(Bundle savedInstaceState){
		if(savedInstaceState != null && savedInstaceState.containsKey("pages")){
			DataStorage.pages = (ArrayList<Page>) savedInstaceState.get("pages");
			DataStorage.selected = (HashMap<String, Integer>) savedInstaceState.get("selected");
			DataStorage.json = savedInstaceState.getString("json");
			
			for(int i = 0; i < DataStorage.pages.size(); i++){
			
			}
			try {
				ParseJSON.parseJSON(DataStorage.json);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
