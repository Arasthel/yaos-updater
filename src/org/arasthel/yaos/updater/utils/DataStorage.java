package org.arasthel.yaos.updater.utils;

import java.util.ArrayList;
import java.util.HashMap;

import org.arasthel.yaos.updater.interfaces.Page;
import org.arasthel.yaos.updater.interfaces.Update;

/**
 * Handy class to store global variables
 */

public class DataStorage {
	
	/** Categories */
	public static ArrayList<Page> pages = new ArrayList<Page>();

	
	/** Stores the current page number */	
	public static int currentPage = 0;
		
		
	/** Selected items in OnlineLists by category.<p>For example: '{"ROM", 2}' */
	public static HashMap<String,Integer> selected = new HashMap<String, Integer>();
	
	
	/**Mirrors by category.<p>For example: '{"ROM", "http://..."}' */
	public static HashMap<String,ArrayList<String>> mirrors = new HashMap<String, ArrayList<String>>();
	
	
	/** Updates by category */
	public static HashMap<String,ArrayList<Update>> updates = new HashMap<String, ArrayList<Update>>();
	
	
	/** Boolean to check if device has a small (<1024 x ...) or big resolution*/	
	public static boolean isTabletScreen = false;
		
		
	/** JSON content */
	public static String json;
	
	/** Download folder */
	public static String downloadPath;
	
	
	/** Update with current version, used to compare current version against online updates' ones */
	public static Update myVersion;
	
	
	/** In case that it's impossible to install files automatically, just reboot in recovery */
	public static Boolean justReboot = false;
	
	
	/** Mountpoint for internal storage in recovery */
	public static String internalRecoveryMountpoint = "/emmc";
	
	/** Path to internal storage in recovery */
	public static String internalRecoveryPath = "/sdcard/";
	
	
	/** Mountpoint for external storage in recovery */
	public static String externalRecoveryMountpoint;
		
	/** Path to external storage in recovery */
	public static String externalRecoveryPath;
	
	
	/** Default JSON URL */
	public static String jsonURL = "";
}
