package org.arasthel.yaos.updater;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import org.arasthel.yaos.updater.utils.Constants;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * The Class SelectDirDialog.
 */
public class SelectDirDialog extends DialogPreference{
	
	/** The current directory. */
	private File curDir;
	
	/** The list of directories. */
	private ListView dirList;
	
	/** The Shared Preferences of the application. */
	private SharedPreferences sp;
	
	/** The new dir edit text. */
	private EditText newDirEditText;
	
	/** The TextView showing current directory. */
	private TextView thisDir;
	
	/** The TextView which shows the current directory in the Preference. */
	private TextView description;
	
	/** The TextView that shows the title of the Preference. */
	private TextView title;

	/**
	 * Instantiates a new select directory dialog.
	 *
	 * @param context the context
	 * @param attrs the attributes
	 */
	public SelectDirDialog(Context context, AttributeSet attrs) {
		super(context, attrs);
		sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		setDialogLayoutResource(R.layout.select_dir_dialog);
	}
	
	/* (non-Javadoc)
	 * @see android.preference.Preference#onBindView(android.view.View)
	 */
	@Override
	protected void onBindView(View view) {
		super.onBindView(view);
		title = (TextView) view.findViewById(android.R.id.title);
		title.setText(super.getDialogTitle());
		description = (TextView) view.findViewById(android.R.id.summary);
		description.setText(getContext().getResources().getString(R.string.current_folder)+sp.getString("download_dir", Constants.downloadDir));
	}
	
	/* (non-Javadoc)
	 * @see android.preference.DialogPreference#onBindDialogView(android.view.View)
	 */
	@Override
	protected void onBindDialogView(View view) {
		super.onBindDialogView(view);
		curDir = new File(sp.getString("download_dir", Constants.downloadDir));
		newDirEditText = (EditText) view.findViewById(R.id.dirName);
		thisDir = (TextView) view.findViewById(R.id.cur_dir_textview);
		dirList = (ListView) view.findViewById(R.id.dirList);
		Button createDir = (Button) view.findViewById(R.id.create_folder_button);
		createDir.setOnClickListener(onCreateClick);
		updateList();
	}
	
	/** The on item click. */
	private OnItemClickListener onItemClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			String newDir = (String) arg0.getAdapter().getItem(arg2);
			if(newDir.equals("..")){
				curDir = curDir.getParentFile();
			}else{
				curDir = new File(curDir.getAbsolutePath()+File.separator+newDir+File.separator);
			}
			updateList();
		}
	};
	
	/** The on create click. */
	private OnClickListener onCreateClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if(v.equals(getDialog().findViewById(R.id.create_folder_button))){
				String newDirStr = newDirEditText.getEditableText().toString();
				if(newDirStr.isEmpty()){
					return;
				}
				File newDir = new File(curDir.getAbsolutePath()+File.separator+newDirStr);
				if(newDir.mkdir()){
					Toast.makeText(getContext(), getContext().getResources().getString(R.string.folder_created), Toast.LENGTH_SHORT).show();
					updateList();
				}else{
					Toast.makeText(getContext(), getContext().getResources().getString(R.string.error_creating_folder), Toast.LENGTH_SHORT).show();
				}
			}
		}
	};
	
	/**
	 * Update list of folders inside the current directory.
	 */
	private void updateList(){
		thisDir.setText(getContext().getResources().getString(R.string.current_folder) + curDir.getAbsolutePath());
		File[] files = curDir.listFiles();
		if(files != null){
			ArrayList<String> filesAL = new ArrayList<String>();
			if(!curDir.getAbsolutePath().equals("/"))
				filesAL.add("..");
			for (int i = 0; i < files.length; i++) {
				if(files[i].isDirectory())
					filesAL.add(files[i].getName());
			}
			Collections.sort(filesAL);
			ArrayAdapter<String> arad = new ArrayAdapter<String>(getContext(), 
					R.layout.list_item_folder, filesAL);
			dirList.setAdapter(arad);
			dirList.setOnItemClickListener(onItemClick);
		}else{
			curDir = curDir.getParentFile();
			updateList();
		}
	}
	
	/* (non-Javadoc)
	 * @see android.preference.DialogPreference#onDialogClosed(boolean)
	 */
	@Override
	protected void onDialogClosed(boolean positiveResult) {
		super.onDialogClosed(positiveResult);
		if(positiveResult == true){
			if(curDir.canWrite()){
				Editor edit = sp.edit();
				edit.putString("download_dir", curDir.getAbsolutePath());
				edit.apply();
				title.setText(super.getTitle());
				description.setText(getContext().getResources().getString(R.string.current_folder)+sp.getString("download_dir", Constants.downloadDir));
				Toast.makeText(getContext(), getContext().getString(R.string.new_current_folder)+curDir.getAbsolutePath(), Toast.LENGTH_SHORT).show();
			}else{
				Toast.makeText(getContext(), getContext().getString(R.string.error_permissions_folder), Toast.LENGTH_SHORT).show();
			}
		}
	}
}
