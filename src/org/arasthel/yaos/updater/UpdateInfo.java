package org.arasthel.yaos.updater;

import java.util.ArrayList;

import org.arasthel.yaos.updater.fragments.DescriptionFragment;
import org.arasthel.yaos.updater.fragments.DetailsFragment;
import org.arasthel.yaos.updater.interfaces.Update;
import org.arasthel.yaos.updater.interfaces.ViewPagerAdapter;
import org.arasthel.yaos.updater.tasks.DownloadFileTask;
import org.arasthel.yaos.updater.utils.DataStorage;
import org.arasthel.yaos.updater.utils.Utils;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * The Class UpdateInfo.
 */
public class UpdateInfo extends FragmentActivity{
	
	/** The page to get info from. */
	private int page = 0;
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Utils.loadPages(savedInstanceState);
		String type = DataStorage.pages.get(DataStorage.currentPage).getType();
		if(DataStorage.selected.containsKey(type)){
			setContentView(R.layout.update_info);
			ViewPager pager = (ViewPager) findViewById(R.id.pager);
			pager.setOnPageChangeListener(new OnPageChangeListener() {
				
				@Override
				public void onPageSelected(int arg0) {
					page = arg0;
					updateDots();
				}
				
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
				}
				
				@Override
				public void onPageScrollStateChanged(int arg0) {
					// TODO Auto-generated method stub
					
				}
			});
			ViewPagerAdapter adapter = new ViewPagerAdapter(this);
			ArrayList<Fragment> fragments = new ArrayList<Fragment>();
			DetailsFragment details = new DetailsFragment();
			fragments.add(details);
			DescriptionFragment description = new DescriptionFragment();
			fragments.add(description);
			adapter.setFragments(fragments);
			pager.setAdapter(adapter);
			updateDots();
		}else{
			setContentView(R.layout.yaos_info);
		}
		
	}
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.info_menu, menu);
        MenuItem item = menu.findItem(R.id.download);
        item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				download();
				return true;
			}
		});
        return true;
	}
	
	/**
	 * Update dots layout to show current page index.
	 */
	private void updateDots(){
		LinearLayout dotContainer = (LinearLayout) findViewById(R.id.dot_container);
		dotContainer.removeAllViews();
		for(int i = 0; i < 2; i++){
			ImageView dot = new ImageView(this);
			if(i == page){
				dot.setImageResource(R.drawable.dot_highlighted);
			}else{
				dot.setImageResource(R.drawable.dot_disabled);
			}
			float scale = getResources().getDisplayMetrics().density;
			int padding = (int) ((int) 4*scale);
			dot.setPadding(padding, 0, padding, 0);
			dotContainer.addView(dot);
		}
	}
	
	/**
	 * Download the current file.
	 *
	 * @return true, if file download starts
	 */
	public boolean download(){
    	if(DataStorage.selected.containsKey(DataStorage.pages.get(DataStorage.currentPage).getType())){
    		int selected = DataStorage.selected.get(DataStorage.pages.get(DataStorage.currentPage).getType());
    		Update update = DataStorage.updates.get(DataStorage.pages.get(DataStorage.currentPage).getType()).get(selected);
    		if(!DownloadFileTask.tasks.containsKey(update.getFilename())){
    			new DownloadFileTask(this, update.getFilename(), DataStorage.pages.get(DataStorage.currentPage).getType()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    		}else{
    			Toast.makeText(this, getResources().getString(R.string.already_downloaded), Toast.LENGTH_SHORT).show();
    		}
    	}
    	return true;
    }
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		Utils.savePages(outState);
		super.onSaveInstanceState(outState);
	}
	
}
