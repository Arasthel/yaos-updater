package org.arasthel.yaos.updater.receivers;

import org.arasthel.yaos.updater.tasks.DownloadFileTask;
import org.arasthel.yaos.updater.utils.Constants;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class CancelDownloadReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context arg0, Intent arg1) {
		if(arg1.getAction().equals(Constants.CANCEL_DOWNLOAD)){
			String filename = arg1.getStringExtra("filename");
			DownloadFileTask dft = DownloadFileTask.tasks.get(filename);
			dft.cancel(false);
		}
		
	}

}
