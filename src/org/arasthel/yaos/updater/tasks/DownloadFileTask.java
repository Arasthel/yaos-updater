package org.arasthel.yaos.updater.tasks;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.arasthel.yaos.updater.R;
import org.arasthel.yaos.updater.utils.Constants;
import org.arasthel.yaos.updater.utils.DataStorage;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

// TODO: Auto-generated Javadoc
/**
 * The Class DownloadFileTask.
 */
public class DownloadFileTask extends AsyncTask<Void, Integer, Boolean>{

	/** A list of tasks of this class */
	public static HashMap<String,DownloadFileTask> tasks = new HashMap<String, DownloadFileTask>();
	
	/** The Constant NOT_VALID_URL. */
	private final static int NOT_VALID_URL = -1;
	
	/** The filename. */
	private String filename;
	
	/** The type (category). */
	private String type;
	
	/** The context. */
	private Context context;
	
	/** The ID of the notification to show. */
	private int notifID;
	
	/** The Notification Manager. */
	private NotificationManager nm;
	
	/** The progress notification. */
	private Notification progressNotification;
	
	/** The last update time (System.currentTimeMilis()). */
	private long lastUpdate = 0;
	
	/**
	 * Instantiates a new download file task.
	 *
	 * @param context the context
	 * @param filename the filename
	 * @param type the type
	 */
	public DownloadFileTask(Context context, String filename, String type) {
		this.filename = filename;
		this.type = type;
		this.context = context;
	}
	
	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onPreExecute()
	 */
	@Override
	protected void onPreExecute() {
		tasks.put(filename, this);
		notifID = new Random().nextInt(Integer.MAX_VALUE);
		nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification.Builder notifBuilder = new Notification.Builder(context);
		Notification notification = notifBuilder.
				setContentTitle(context.getString(R.string.downloading_file)).
				setSmallIcon(R.drawable.ic_launcher).
				setContentText(context.getString(R.string.file_name)+filename).getNotification();
		nm.notify(notifID, notification);
		Intent i = new Intent(Constants.DOWNLOAD_START);
		context.sendBroadcast(i);
		super.onPreExecute();
	}

	/* (non-Javadoc)
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	protected Boolean doInBackground(Void... params) {
		String path = PreferenceManager.getDefaultSharedPreferences(context).getString("download_dir", Constants.downloadDir);
		ArrayList<String> mirrors = DataStorage.mirrors.get(type);
		if(mirrors.isEmpty()){
			publishProgress(NOT_VALID_URL);
			return null;
		}
		URL validURL = null;
		for(String mirror : mirrors){
			if(!mirror.endsWith("/")){
				mirror += "/";
			}
			try {
				URL testURL = new URL(mirror+filename);
				if(urlExists(testURL)){
					validURL = testURL;
					break;
				}
			} catch (SocketTimeoutException e){
				Log.d("DOWNLOAD",filename+" TIMEOUT");
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(validURL == null){
			publishProgress(NOT_VALID_URL);
			cancel(true);
			return null;
		}
		try {
			URLConnection connection = validURL.openConnection();
			connection.setConnectTimeout(10000);
			connection.connect();
			Log.d("STARTED","DOWNLOAD: "+filename);
			int fileSize = connection.getContentLength();
			int onePercent = fileSize/100;
			BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
			if(!path.endsWith("/")){
				path += "/";
			}
			path += type + "/";
			File folder = new File(path);
			if(!folder.exists()){
				folder.mkdir();
			}
			FileOutputStream fos = new FileOutputStream(new File(path,filename+".part"));
			byte[] buffer = new byte[1024];
			int count;
			int downloadedData = 0;
			int percent = 0;
			while((count = bis.read(buffer, 0, buffer.length)) > 0 && !this.isCancelled()){
				fos.write(buffer, 0, count);
				downloadedData += count;
				while(downloadedData >= onePercent){
					percent++;
					downloadedData -= onePercent;
					if(System.currentTimeMillis()-lastUpdate >= 2000){
						publishProgress(percent);
						lastUpdate = System.currentTimeMillis();
					}
				}
			}
			fos.close();
			bis.close();
			File file = new File(path, filename+".part");
			file.renameTo(new File(path, filename));
			connection = null;
			if(!this.isCancelled()){
				URL md5URL = new URL(validURL.toExternalForm()+".md5");
				if(urlExists(md5URL)){
					BufferedReader br = new BufferedReader(new InputStreamReader(md5URL.openStream()));
					FileWriter fw = new FileWriter(new File(path,filename+".md5"));
					fw.write(br.readLine());
					fw.close();
					br.close();
				}
			}
			tasks.remove(filename);
			if(this.isCancelled()){
				File f = new File(path,filename);
				if(f.exists()){
					f.delete();
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(Boolean result) {
		if(isCancelled()){
			return;
		}
		nm.cancel(notifID);
		Intent activity = new Intent(context,org.arasthel.yaos.updater.Main.class);
		PendingIntent pIntent = PendingIntent.getActivity(context, 0, activity, PendingIntent.FLAG_UPDATE_CURRENT | Notification.FLAG_AUTO_CANCEL);
		Notification.Builder notifBuilder = new Notification.Builder(context);
		Notification notification = notifBuilder
				.setContentTitle(context.getString(R.string.download_finished))
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentText(filename)
				.setAutoCancel(true)
				.setContentIntent(pIntent).getNotification();
		nm.notify(notifID, notification);
		Intent i = new Intent(Constants.REFRESH_LIST);
		context.sendBroadcast(i);
		super.onPostExecute(result);
	}
	
	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onProgressUpdate(Progress[])
	 */
	@Override
	protected void onProgressUpdate(Integer... values) {
		int value = values[0];
		if(value < 0){
			switch(value){
			case(NOT_VALID_URL):
				Toast.makeText(context, context.getString(R.string.error_wrong_url), Toast.LENGTH_SHORT).show();
				break;
			}
		}else{
			if(progressNotification == null){
				progressNotification = new Notification();
				progressNotification.flags = Notification.FLAG_ONGOING_EVENT;
				progressNotification.contentView = new RemoteViews(context.getPackageName(),R.layout.progress_notification);
				progressNotification.icon = R.drawable.ic_launcher;
				progressNotification.contentView.setTextViewText(R.id.notif_title, filename);
				progressNotification.contentView.setImageViewResource(R.id.notifIcon, R.drawable.ic_launcher);
				Intent i = new Intent();
				i.setAction(Constants.CANCEL_DOWNLOAD);
				i.putExtra("filename", filename);
				progressNotification.contentIntent = PendingIntent.getBroadcast(context, notifID, i, PendingIntent.FLAG_ONE_SHOT);
			}
			progressNotification.contentView.setTextViewText(R.id.textPercent, value+"%");
			progressNotification.contentView.setProgressBar(R.id.downloadProgressBar, 100, value, false);
			nm.notify(notifID, progressNotification);
		}
		super.onProgressUpdate(values);
	}
	
	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onCancelled()
	 */
	@Override
	protected void onCancelled() {
		nm.cancel(notifID);
		Notification.Builder notifBuilder = new Notification.Builder(context);
		Notification notification = notifBuilder.
				setContentTitle(context.getString(R.string.download_canceled)).
				setSmallIcon(R.drawable.ic_launcher).
				setContentText(filename).getNotification();
		nm.notify(notifID, notification);
		tasks.remove(filename);
		super.onCancelled();
	}
	
	/**
	 * Url exists.
	 *
	 * @param url the url to check
	 * @return true, if the url exists
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SocketTimeoutException the socket timeout exception
	 */
	public static boolean urlExists(URL url) throws IOException, SocketTimeoutException{
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("HEAD");
		connection.setConnectTimeout(10000);
		connection.connect();
		int code = connection.getResponseCode();
		connection.disconnect();
		if(code == HttpURLConnection.HTTP_OK){
			return true;
		}else{
			return false;
		}
	}
}
