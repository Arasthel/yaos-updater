package org.arasthel.yaos.updater.tasks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.arasthel.yaos.updater.R;
import org.arasthel.yaos.updater.fragments.DetailsFragment;
import org.arasthel.yaos.updater.interfaces.Update;
import org.arasthel.yaos.updater.utils.DataStorage;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

public class GetMD5Task extends AsyncTask<Void, Integer, Boolean> {

	private String path;
	private Context context;
	private String type;
	private Update update;
	
	public GetMD5Task(Context context, String path, Update update, String type) {
		this.path = path;
		this.context = context;
		this.type = type;
		this.update = update;
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		if(!path.endsWith("/")){
			path += "/";
		}
		path += type + "/";
		File folder = new File(path);
		if(!folder.exists()){
			folder.mkdir();
		}
		File md5 = new File(path+update.getFilename()+".md5");
		try {
			ArrayList<String> mirrors = DataStorage.mirrors.get(type);
			if(mirrors == null){
				publishProgress(-1);
				return false;
			}
			if(mirrors.isEmpty()){
				publishProgress(-1);
				return false;
			}
			URL validURL = null;
			for(String mirror : mirrors){
				if(!mirror.endsWith("/")){
					mirror += "/";
				}
				try {
					URL testURL = new URL(mirror+update.getFilename()+".md5");
					if(DownloadFileTask.urlExists(testURL)){
						validURL = testURL;
						break;
					}
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(validURL == null){
				publishProgress(-1);
				return false;
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(validURL.openStream()));
			FileWriter fw = new FileWriter(md5);
			fw.write(br.readLine());
			fw.close();
			br.close();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		int value = values[0];
		switch(value){
		case(-1):
			Toast.makeText(context, context.getString(R.string.error_wrong_url), Toast.LENGTH_SHORT).show();
		break;
		}
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		if(result){
			Toast.makeText(context, context.getString(R.string.md5_download_sucessful), Toast.LENGTH_SHORT).show();
			DetailsFragment details = (DetailsFragment) ((FragmentActivity) context).getSupportFragmentManager().findFragmentById(R.id.details);
			if(details != null){
				details.updateContent(update);
			}
		}
		super.onPostExecute(result);
	}
	
	
}
