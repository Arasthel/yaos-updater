package org.arasthel.yaos.updater.tasks;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.arasthel.yaos.updater.R;
import org.arasthel.yaos.updater.utils.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class CheckMD5Task extends AsyncTask<Void, Void, Boolean>{

	private String path;
	private Context context;
	private ProgressDialog dialog;
	
	public CheckMD5Task(Context context, String path){
		this.context = context;
		this.path = path;
	}
	
	@Override
	protected void onPreExecute() {
		dialog = new ProgressDialog(context);
		dialog.setMessage(context.getString(R.string.checking_md5));
		dialog.setIndeterminate(true);
		dialog.setCancelable(false);
		dialog.show();
		super.onPreExecute();
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
		try {
			return Utils.checkMD5(path);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		dialog.cancel();
		if(result){
			Toast.makeText(context, context.getString(R.string.md5_good), Toast.LENGTH_LONG).show();
		}else{
			Toast.makeText(context, context.getString(R.string.md5_bad), Toast.LENGTH_LONG).show();
		}
		super.onPostExecute(result);
	}

}
