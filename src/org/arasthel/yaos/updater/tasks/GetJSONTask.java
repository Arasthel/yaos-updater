package org.arasthel.yaos.updater.tasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.arasthel.yaos.updater.R;
import org.arasthel.yaos.updater.utils.Constants;
import org.arasthel.yaos.updater.utils.DataStorage;
import org.arasthel.yaos.updater.utils.ParseJSON;
import org.arasthel.yaos.updater.utils.Utils;
import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

public class GetJSONTask extends AsyncTask<Context, Integer, String> {
	
	private final static int BAD_URL = 1;
	private final static int IO_EX = 2;
	private final static int JSON_EX = 3;
	private final static int NO_INTERNET = 4;
	
	private ProgressDialog dialog;
	
	private Context context;

	public GetJSONTask(ProgressDialog dialog) {
		this.dialog = dialog;
	}
	
	@Override
	protected String doInBackground(Context... params) {
		context = params[0];
		if(!Utils.isConnectionEnabled(context)){
			publishProgress(NO_INTERNET);
			return null;
		}
		try {
			URL jsonUrl = new URL(DataStorage.jsonURL);
			BufferedReader br = new BufferedReader(new InputStreamReader(jsonUrl.openConnection().getInputStream()));
			StringBuilder strBuilder = new StringBuilder();
			String line;
			while((line = br.readLine()) != null){
				strBuilder.append(line);
			}
			br.close();
			String jsonStr = strBuilder.toString();
			if(jsonStr == null){
				return null;
			}
			ParseJSON.parseJSON(jsonStr);
			Intent i = new Intent();
			i.setAction(Constants.REFRESH_LIST);
			context.sendBroadcast(i);
			return jsonStr;
		} catch (MalformedURLException e) {
			publishProgress(BAD_URL);
			e.printStackTrace();
		} catch (IOException e) {
			publishProgress(IO_EX);
			e.printStackTrace();
		} catch (JSONException e) {
			publishProgress(JSON_EX);
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		int value = values[0];
		switch(value){
		case BAD_URL:
			Toast.makeText(context, context.getString(R.string.error_json_badurl), Toast.LENGTH_SHORT).show();
			break;
		case IO_EX:
			Toast.makeText(context, context.getString(R.string.error_wrong_url), Toast.LENGTH_SHORT).show();
			break;
		case JSON_EX:
			Toast.makeText(context, context.getString(R.string.error_json_format), Toast.LENGTH_SHORT).show();
			break;
		case NO_INTERNET:
			Toast.makeText(context, context.getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
			break;
		}
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(String result) {
		dialog.cancel();
		DataStorage.json = result;
		super.onPostExecute(result);
	}

}
