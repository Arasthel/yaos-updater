package org.arasthel.yaos.updater.tasks;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.arasthel.yaos.updater.R;
import org.arasthel.yaos.updater.interfaces.InstallListItem;
import org.arasthel.yaos.updater.utils.Constants;
import org.arasthel.yaos.updater.utils.DataStorage;
import org.arasthel.yaos.updater.utils.Utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class ApplyUpdateTask extends AsyncTask<Context, String, Boolean>{

	private SharedPreferences sp;
	private Context context;
	private ProgressDialog progress;
	private ArrayList<InstallListItem> items;
	private boolean openRecoverySupport = false;
	
	public ApplyUpdateTask(Context context, ArrayList<InstallListItem> items) {
		this.context = context;
		this.items = items;
		sp = PreferenceManager.getDefaultSharedPreferences(context);
		openRecoverySupport = sp.getBoolean("openrecovery_support", false);
	}
	
	@Override
	protected void onPreExecute() {
		if(sp.getBoolean("check_md5", true)){
			progress = new ProgressDialog(context);
			progress.setIndeterminate(true);
			progress.setMessage(context.getString(R.string.checking_md5));
			progress.setCancelable(false);
			progress.show();
		}
		super.onPreExecute();
	}
	
	private void reboot(){
		progress.cancel();
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(context.getString(R.string.device_will_reboot));
		builder.setPositiveButton(context.getString(R.string.accept), new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				try {
					Process su = Runtime.getRuntime().exec("su");
					OutputStreamWriter oow = new OutputStreamWriter(su.getOutputStream());
					oow.write("reboot recovery\nexit\n");
					oow.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		builder.setNegativeButton(context.getString(R.string.cancel), new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				File cache = new File("/cache/");
				if(cache.canWrite()){
					File extended;
					if(openRecoverySupport){
						extended = new File("/cache/recovery/openrecoveryscript");
					}else{
						extended = new File("/cache/recovery/extendedcommand");
					}
					extended.delete();
				}else{
					Process su;
					try {
						su = Runtime.getRuntime().exec("su");
						OutputStreamWriter oow = new OutputStreamWriter(su.getOutputStream());
						if(openRecoverySupport){
							oow.write("rm /cache/recovery/openrecoveryscript\nexit\n");
						}else{
							oow.write("rm /cache/recovery/extendedcommand\nexit\n");
						}
						oow.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		builder.show();
	}

	@Override
	protected Boolean doInBackground(Context... arg0) {
		context = arg0[0];
		if(!(items != null && items.size() > 0)){
			return true;
		}else{
			StringBuilder commands = new StringBuilder();
			String downloadDir = sp.getString("download_dir", Constants.downloadDir);
			if(downloadDir.matches("/mnt/[a-zA-Z0-9/]*")){
				downloadDir = downloadDir.replace("/mnt/", "/");
			}
			if(downloadDir.matches("/storage/sdcard[a-zA-Z0-9/]*")){
				downloadDir = downloadDir.replace("/storage/sdcard0", "/");
				downloadDir = downloadDir.replace("/storage/sdcard1", "/");
			}
			if(!downloadDir.endsWith("/")){
				downloadDir += "/";
			}
			if(!downloadDir.matches(DataStorage.internalRecoveryPath+"[/]*")){
				downloadDir = DataStorage.internalRecoveryPath+downloadDir;
			}	
			if(openRecoverySupport){	
				commands.append("mount "+DataStorage.internalRecoveryMountpoint+"\n");
			}else{
				commands.append("run_program(\"/sbin/mount "+DataStorage.internalRecoveryMountpoint+"\");\n");
			}
			boolean checkMD5 = sp.getBoolean("check_md5", true);
			if(checkMD5){
				boolean isMD5ok = true;
				for(InstallListItem item : items){
					switch(item.getType()){
					case InstallListItem.FILE:
						// ITEM is a FILE
						if(!isMD5ok){
							break;
						}
						try {
							String file = downloadDir+item.getCategory().getType()+"/"+item.getName();
							isMD5ok = Utils.checkMD5(file);
							if(!isMD5ok){
								publishProgress(item.getName());
							}else{
								file.replaceAll(" ", "\\ ");
								if(openRecoverySupport){
									commands.append("install "+file+"\n");
								}else{
									commands.append("install_zip(\""+file+"\");\n");
								}
							}
						} catch (NoSuchAlgorithmException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
						
					case InstallListItem.WIPE:
						// ITEM is a WIPE
						if(openRecoverySupport){
							commands.append("wipe data\n");
						}else{
							commands.append("format(\"/data\");\n");
						}
						break;
						
					case InstallListItem.BACKUP:
						//ITEM is a BACKUP
						String name = item.getName();
						name.replaceAll(" ", "\\ ");
						if(openRecoverySupport){
							commands.append("backup SDBO "+item.getName()+"\n");
						}else{
							commands.append("backup_rom(\""+DataStorage.internalRecoveryPath+"/clockworkmod/backup/"+item.getName()+"\");\n");
						}
						break;
					}
				}
				if(!isMD5ok){
					return false;
				}
				if(new File("/cache").canWrite()){
					FileWriter fw;
					try {
						File cacheFile;
						if(openRecoverySupport){
							cacheFile = new File("/cache/recovery/openrecoveryscript");
						}else{
							cacheFile = new File("/cache/recovery/extendedcommand");
						}
						if(!cacheFile.exists()){
							cacheFile.createNewFile();
						}
						fw = new FileWriter(cacheFile);
						fw.write(commands.toString());
						fw.flush();
						fw.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					
					try {
						Process su = Runtime.getRuntime().exec("su");
						OutputStreamWriter oow = new OutputStreamWriter(su.getOutputStream());
						String file;
						if(openRecoverySupport){
							file = "/cache/recovery/openrecoveryscript";
						}else{
							file = "/cache/recovery/extendedcommand";
						}
						oow.write("echo '"+commands.toString()+"' > "+file+"\nexit\n");
						oow.flush();
						oow.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				return true;
			}
			
		}
		return false;
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		reboot();
		super.onPostExecute(result);
	}
	
	@Override
	protected void onProgressUpdate(String... values) {
		Toast.makeText(context, context.getString(R.string.error_checksum)+values[0], Toast.LENGTH_SHORT).show();
		progress.cancel();
		super.onProgressUpdate(values);
	}
	
}
