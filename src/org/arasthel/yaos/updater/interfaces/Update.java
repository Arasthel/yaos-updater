package org.arasthel.yaos.updater.interfaces;

import java.util.ArrayList;
import java.util.StringTokenizer;

import org.arasthel.yaos.updater.utils.Constants;

public class Update implements Comparable<Update>{

	private String name;
	private ArrayList<Integer> version;
	private ArrayList<Integer> versionMin;
	private String filename;
	private boolean stable;
	private String description;
	private String uploader;
	private Page category;
	
	public Update(String name, String version, String versionMin, String filename, boolean stable, String description, String uploader, Page page) {
		this.name = name;
		this.filename = filename;
		this.stable = stable;
		this.description = description;
		this.uploader = uploader;
		// Version translation from String to List of integers
		this.version = parseVersionStr(version);
		
		//Needed if update is a patch
		this.versionMin = parseVersionStr(versionMin);
		this.category = page;
	}
	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public ArrayList<Integer> getVersion() {
		return version;
	}



	public void setVersion(ArrayList<Integer> version) {
		this.version = version;
	}



	public ArrayList<Integer> getVersionMin() {
		return versionMin;
	}



	public void setVersionMin(ArrayList<Integer> versionMin) {
		this.versionMin = versionMin;
	}


	public String getFilename() {
		return filename;
	}



	public void setFilename(String filename) {
		this.filename = filename;
	}



	public boolean isStable() {
		return stable;
	}



	public void setStable(boolean stable) {
		this.stable = stable;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}

	public String getUploader() {
		return uploader;
	}

	public void setUploader(String uploader) {
		this.uploader = uploader;
	}

	public Page getCategory() {
		return category;
	}

	public void setCategory(Page p) {
		this.category = p;
	}

	@Override
	public int compareTo(Update other) {
		int size = Math.min(version.size(), other.getVersion().size());
		if(versionMin == null){		
			for(int i = 0; i < size; i++){
				if(version.get(i) > other.getVersion().get(i)){
					return 1;
				}else if(version.get(i) < other.getVersion().get(i)){
					return -1;
				}else{
					if(i == size-1){
						return 0;
					}else{
						continue;
					}
				}
			}
		}else{
			for(int i = 0; i < size; i++){
				if(versionMin.get(i) > other.getVersion().get(i)){
					return 1;
				}else if(version.get(i) < other.getVersion().get(i)){
					return -1;
				}else{
					if(i == size-1){
						return 0;
					}else{
						continue;
					}
				}
			}
		}
		return 0;
	}
	
	public static ArrayList<Integer> parseVersionStr(String versionStr){
		if(versionStr == null){
			return null;
		}
		ArrayList<Integer> version = new ArrayList<Integer>();
		StringTokenizer tokenizer = new StringTokenizer(versionStr,Constants.versionSeparator);
		while(tokenizer.hasMoreTokens()){
			version.add(Integer.parseInt(tokenizer.nextToken()));
		}
		if(version.isEmpty()){
			return null;
		}
		return version;
	}
	
	public String getVersionString(ArrayList<Integer> version){
		StringBuilder str = new StringBuilder();
		for(int i = 0; i < version.size(); i++){
			str.append(version.get(i));
			if(i < version.size()-1){
				str.append(Constants.versionSeparator);
			}
		}
		return str.toString();
	}
	
	public boolean canApplyTo(Update other){
		if(versionMin != null){
			return (this.compareTo(other) == 0);
		}else{
			return (this.compareTo(other) == 1);
		}
	}
	
	@Override
	public String toString() {
		return this.name;
	}
}
