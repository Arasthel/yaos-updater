package org.arasthel.yaos.updater.interfaces;

public class InstallListItem {

	public static final int FILE = 0;
	public static final int WIPE = 1;
	public static final int BACKUP = 2;
	
	private int type;
	private Page category;
	private String name;
	
	public InstallListItem(int type, String name, Page category) {
		this.type = type;
		this.name = name;
		this.category = category;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Page getCategory() {
		return category;
	}

	public void setCategory(Page category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
