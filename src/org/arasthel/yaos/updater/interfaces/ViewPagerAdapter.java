package org.arasthel.yaos.updater.interfaces;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

public class ViewPagerAdapter extends FragmentStatePagerAdapter{

	private SparseArray<Fragment> fragments = new SparseArray<Fragment>();
	
	public ViewPagerAdapter(Context context) {
		super(((FragmentActivity) context).getSupportFragmentManager());
	}

	@Override
	public Fragment getItem(int i) {
		return fragments.get(i);
	}
	
	public Fragment getFragment(int i){
		return fragments.get(i);
	}

	@Override
	public int getCount() {
		return fragments.size();
	}

	public SparseArray<Fragment> getFragments() {
		return fragments;
	}

	public void setFragments(ArrayList<Fragment> fragments) {
		for(int i = 0; i < fragments.size(); i++){
			this.fragments.put(i, fragments.get(i));
		}
	}
	
	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}
	
}
