package org.arasthel.yaos.updater.interfaces;

import java.io.Serializable;

public class Page implements Serializable {

	private static final long serialVersionUID = -3457478803295376174L;
	private String type;
	private int index;
	private String name;
	
	public Page(String type, int index, String name) {
		this.type = type;
		this.index = index;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return index+": "+name+"|"+type;
	}
	
}
