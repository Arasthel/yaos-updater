package org.arasthel.yaos.updater.interfaces;

import org.arasthel.yaos.updater.R;
import org.arasthel.yaos.updater.utils.DataStorage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomHighLightAdapter<T> extends ArrayAdapter<T>{

	private Context context;
	private String type;
	private int selected;
	private int resource;
	
	public CustomHighLightAdapter(Context context, int textViewResourceId, String type) {
		super(context, textViewResourceId);
		this.context = context;
		this.type = type;
		this.resource = textViewResourceId;
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(resource, null);
		}
		TextView text = (TextView) convertView.findViewById(android.R.id.text1);
		Update update = (Update) super.getItem(position);
		text.setText(update.getName());
		selected = -1;
		if(DataStorage.selected.containsKey(type)){
			selected = DataStorage.selected.get(type);
		}
		if(selected == position){
			if(DataStorage.isTabletScreen){
				convertView.setBackgroundResource(R.drawable.list_item_selected);
			}else{
				convertView.setBackgroundResource(android.R.color.holo_blue_dark);
			}
		}else{
			convertView.setBackgroundResource(android.R.color.transparent);
		}
		return convertView;
	}
	
	public void setType(String type){
		this.type = type;
		notifyDataSetChanged();
	}

}
