package org.arasthel.yaos.updater.interfaces;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.arasthel.yaos.updater.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class InstallListAdapter extends ArrayAdapter<InstallListItem>{
	
	ArrayList<InstallListItem> items;
	private Context context;

	public InstallListAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
		this.context = context;
		items = new ArrayList<InstallListItem>();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.list_item, null);
		}
		TextView name = (TextView) convertView.findViewById(android.R.id.text1);
		InstallListItem item = (InstallListItem) items.get(position);
		switch (item.getType()) {
		case InstallListItem.FILE:
			name.setText(item.getCategory().getName()+"/"+item.getName());
			break;
		case InstallListItem.WIPE:
			name.setText(context.getString(R.string.wipe));
			break;
		case InstallListItem.BACKUP:
			name.setText(context.getString(R.string.backup)+": "+item.getName());
			break;
		}
		return convertView;
	}

	public boolean moveItemToPosition(int from, int to){
		if(from >= 0 && from < getCount() && to >= 0 && to < getCount()){
			Collections.swap(items, from, to);
			notifyDataSetInvalidated();
			return true;
		}
		return false;
	}
	
	@Override
	public void add(InstallListItem object) {
		items.add(object);
		notifyDataSetChanged();
	}
	
	@Override
	public void addAll(InstallListItem... objects) {
		for(InstallListItem o : objects){
			items.add(o);
		}
		notifyDataSetChanged();
	}
	
	@Override
	public void addAll(Collection<? extends InstallListItem> collection) {
		for(InstallListItem o : collection){
			items.add(o);
		}
		notifyDataSetChanged();
	}
	
	@Override
	public void remove(InstallListItem object) {
		items.remove(object);
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return items.size();
	}
	
	@Override
	public boolean isEmpty() {
		return items.isEmpty();
	}
	
	public boolean remove(int i){
		if(items.remove(i) != null){
			notifyDataSetChanged();
			return true;
		}
		return false;
	}
	
	public ArrayList<InstallListItem> getList(){
		return items;
	}
	
	
}
