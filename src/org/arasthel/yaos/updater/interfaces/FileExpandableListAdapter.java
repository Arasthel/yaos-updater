package org.arasthel.yaos.updater.interfaces;

import java.io.File;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;

import org.arasthel.yaos.updater.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

public class FileExpandableListAdapter extends BaseExpandableListAdapter{
	
	private Page[] groups;
	private File[][] items;
	private Context context;
	private ArrayList<SimpleEntry<String, String>> checked;
	
	public FileExpandableListAdapter(Context context, Page[] categories, File[][] items) {
		this.groups = categories;
		this.items = items;
		this.context = context;
		checked = new ArrayList<SimpleEntry<String , String>>();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return items[groupPosition][childPosition];
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(R.layout.install_list_item, null);
		}
		CheckedTextView name = (CheckedTextView) convertView.findViewById(android.R.id.text1);
		name.setText(items[groupPosition][childPosition].getName());
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		int i = 0;
		try{
			i = items[groupPosition].length;
		}catch(Exception e){
			
		}
		return i;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return groups[groupPosition];
	}

	@Override
	public int getGroupCount() {
		return groups.length;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(R.layout.file_list_group_item, null);
		}
		TextView category = (TextView) convertView;
		category.setTextSize(20);
		category.setText(groups[groupPosition].getName());
		return convertView;
	}
	
	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	public Page[] getGroups() {
		return groups;
	}

	public void setGroups(Page[] groups) {
		this.groups = groups;
	}

	public File[][] getItems() {
		return items;
	}

	public void setItems(File[][] items) {
		this.items = items;
	}

	public ArrayList<SimpleEntry<String, String>> getChecked() {
		return checked;
	}

	public void setChecked(ArrayList<SimpleEntry<String, String>> checked) {
		this.checked = checked;
	}

}
